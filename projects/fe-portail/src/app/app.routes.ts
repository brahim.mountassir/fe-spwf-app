import { Routes } from '@angular/router';
import { HomeComponent } from './portail/components/home/home.component';

export const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./portail/portail-routing.module').then(m => m.PortailRoutingModule),
  },
];
